﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscDiscountTypeMaster
    {
        public AscDiscountTypeMaster()
        {
            AscDiscountOrder = new HashSet<AscDiscountOrder>();
        }

        public int Id { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public double Percentage { get; set; }

        public ICollection<AscDiscountOrder> AscDiscountOrder { get; set; }
    }
}
