﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscOrderLineItem
    {
        public int Id { get; set; }
        public int Qty { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
        public int ItemNo { get; set; }
        public string Comments { get; set; }
        public string Measure { get; set; }
        public int AscDiscountOrderId { get; set; }

        public AscDiscountOrder AscDiscountOrder { get; set; }
    }
}
