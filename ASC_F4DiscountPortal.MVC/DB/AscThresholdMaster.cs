﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscThresholdMaster
    {
        public AscThresholdMaster()
        {
            AscUserApprovalMaster = new HashSet<AscUserApprovalMaster>();
        }

        public int Id { get; set; }
        public int Min { get; set; }
        public int Max { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public ICollection<AscUserApprovalMaster> AscUserApprovalMaster { get; set; }
    }
}
