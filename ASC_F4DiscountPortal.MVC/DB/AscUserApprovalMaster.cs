﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscUserApprovalMaster
    {
        public string ApproverEmail { get; set; }
        public int AscUserId { get; set; }
        public int AscThresholdMasterId { get; set; }
        public int Id { get; set; }

        public AscThresholdMaster AscThresholdMaster { get; set; }
        public AscUserMaster AscUser { get; set; }
    }
}
