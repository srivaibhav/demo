﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscDiscountOrder
    {
        public AscDiscountOrder()
        {
            AscApprovalTransaction = new HashSet<AscApprovalTransaction>();
            AscOrderLineItem = new HashSet<AscOrderLineItem>();
        }

        public int Id { get; set; }
        public byte[] IsSplit { get; set; }
        public DateTime ShipDate { get; set; }
        public string Status { get; set; }
        public decimal Percentage { get; set; }
        public decimal Value { get; set; }
        public string CustName { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public int AscRegionId { get; set; }
        public int AscDiscountTypeId { get; set; }
        public int AscVendorId { get; set; }
        public int AscUserId { get; set; }
        public string OrderNumber { get; set; }
        public decimal DiscountedValue { get; set; }

        public AscDiscountTypeMaster AscDiscountType { get; set; }
        public AscRegionMaster AscRegion { get; set; }
        public AscUserMaster AscUser { get; set; }
        public AscVendorMaster AscVendor { get; set; }
        public ICollection<AscApprovalTransaction> AscApprovalTransaction { get; set; }
        public ICollection<AscOrderLineItem> AscOrderLineItem { get; set; }
    }
}
