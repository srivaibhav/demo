﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscVendorMaster
    {
        public AscVendorMaster()
        {
            AscDiscountOrder = new HashSet<AscDiscountOrder>();
            AscPromocodeMaster = new HashSet<AscPromocodeMaster>();
            AscUserMaster = new HashSet<AscUserMaster>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public int RegionId { get; set; }
        public string Name { get; set; }

        public ICollection<AscDiscountOrder> AscDiscountOrder { get; set; }
        public ICollection<AscPromocodeMaster> AscPromocodeMaster { get; set; }
        public ICollection<AscUserMaster> AscUserMaster { get; set; }
    }
}
