﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class F4DsicountPortalContext : DbContext
    {
        public F4DsicountPortalContext()
        {
        }

        public F4DsicountPortalContext(DbContextOptions<F4DsicountPortalContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AscApprovalTransaction> AscApprovalTransaction { get; set; }
        public virtual DbSet<AscDiscountOrder> AscDiscountOrder { get; set; }
        public virtual DbSet<AscDiscountTypeMaster> AscDiscountTypeMaster { get; set; }
        public virtual DbSet<AscOrderLineItem> AscOrderLineItem { get; set; }
        public virtual DbSet<AscPromocodeMaster> AscPromocodeMaster { get; set; }
        public virtual DbSet<AscRegionMaster> AscRegionMaster { get; set; }
        public virtual DbSet<AscRoleMaster> AscRoleMaster { get; set; }
        public virtual DbSet<AscThresholdMaster> AscThresholdMaster { get; set; }
        public virtual DbSet<AscUserApprovalMaster> AscUserApprovalMaster { get; set; }
        public virtual DbSet<AscUserMaster> AscUserMaster { get; set; }
        public virtual DbSet<AscVendorMaster> AscVendorMaster { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=VSRIVASTAV01;Database=F4DsicountPortal;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AscApprovalTransaction>(entity =>
            {
                entity.ToTable("ASC_ApprovalTransaction");

                entity.Property(e => e.ApproverEmail).IsRequired();

                entity.Property(e => e.AscDiscountOrderId).HasColumnName("ASC_DiscountOrder_Id");

                entity.Property(e => e.Comments).IsRequired();

                entity.Property(e => e.CreatedBy).IsRequired();

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).IsRequired();

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");

                entity.Property(e => e.RequestedBy).IsRequired();

                entity.Property(e => e.Status).IsRequired();

                entity.HasOne(d => d.AscDiscountOrder)
                    .WithMany(p => p.AscApprovalTransaction)
                    .HasForeignKey(d => d.AscDiscountOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Approval_ASC_DiscountOrder");
            });

            modelBuilder.Entity<AscDiscountOrder>(entity =>
            {
                entity.ToTable("ASC_DiscountOrder");

                entity.Property(e => e.AscDiscountTypeId).HasColumnName("ASC_DiscountType_Id");

                entity.Property(e => e.AscRegionId).HasColumnName("ASC_Region_Id");

                entity.Property(e => e.AscUserId).HasColumnName("ASC_User_Id");

                entity.Property(e => e.AscVendorId).HasColumnName("Asc_Vendor_Id");

                entity.Property(e => e.CustCity)
                    .IsRequired()
                    .HasColumnName("Cust_City");

                entity.Property(e => e.CustName)
                    .IsRequired()
                    .HasColumnName("Cust_Name");

                entity.Property(e => e.CustState)
                    .IsRequired()
                    .HasColumnName("Cust_State");

                entity.Property(e => e.DiscountedValue).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.IsSplit)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.OrderNumber).IsRequired();

                entity.Property(e => e.Percentage).HasColumnType("decimal(18, 0)");

                entity.Property(e => e.ShipDate).HasColumnType("datetime");

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Value).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.AscDiscountType)
                    .WithMany(p => p.AscDiscountOrder)
                    .HasForeignKey(d => d.AscDiscountTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_DiscountOrder_ASC_DiscountType");

                entity.HasOne(d => d.AscRegion)
                    .WithMany(p => p.AscDiscountOrder)
                    .HasForeignKey(d => d.AscRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_DiscountOrder_ASC_Region");

                entity.HasOne(d => d.AscUser)
                    .WithMany(p => p.AscDiscountOrder)
                    .HasForeignKey(d => d.AscUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_DiscountOrder_ASC_User");

                entity.HasOne(d => d.AscVendor)
                    .WithMany(p => p.AscDiscountOrder)
                    .HasForeignKey(d => d.AscVendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_DiscountOrder_Asc_Vendor");
            });

            modelBuilder.Entity<AscDiscountTypeMaster>(entity =>
            {
                entity.ToTable("ASC_DiscountTypeMaster");

                entity.Property(e => e.Category).IsRequired();

                entity.Property(e => e.Percentage).HasColumnName("percentage");

                entity.Property(e => e.Type).IsRequired();
            });

            modelBuilder.Entity<AscOrderLineItem>(entity =>
            {
                entity.ToTable("ASC_OrderLineItem");

                entity.Property(e => e.AscDiscountOrderId).HasColumnName("ASC_DiscountOrder_Id");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Comments).IsRequired();

                entity.Property(e => e.Measure)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 0)");

                entity.HasOne(d => d.AscDiscountOrder)
                    .WithMany(p => p.AscOrderLineItem)
                    .HasForeignKey(d => d.AscDiscountOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("OrderLineItem_ASC_DiscountOrder");
            });

            modelBuilder.Entity<AscPromocodeMaster>(entity =>
            {
                entity.ToTable("ASC_PromocodeMaster");

                entity.Property(e => e.AscVendorId).HasColumnName("Asc_Vendor_Id");

                entity.Property(e => e.Promocode)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.AscVendor)
                    .WithMany(p => p.AscPromocodeMaster)
                    .HasForeignKey(d => d.AscVendorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASc_Promocode_Asc_Vendor");
            });

            modelBuilder.Entity<AscRegionMaster>(entity =>
            {
                entity.ToTable("ASC_RegionMaster");

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<AscRoleMaster>(entity =>
            {
                entity.ToTable("ASC_RoleMaster");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(100);

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<AscThresholdMaster>(entity =>
            {
                entity.ToTable("ASC_ThresholdMaster");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ModifiedDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<AscUserApprovalMaster>(entity =>
            {
                entity.ToTable("ASC_User_ApprovalMaster");

                entity.Property(e => e.ApproverEmail)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.AscThresholdMasterId).HasColumnName("ASC_ThresholdMaster_Id");

                entity.Property(e => e.AscUserId).HasColumnName("ASC_User_Id");

                entity.HasOne(d => d.AscThresholdMaster)
                    .WithMany(p => p.AscUserApprovalMaster)
                    .HasForeignKey(d => d.AscThresholdMasterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_User_Approval_ASC_ThresholdMaster");

                entity.HasOne(d => d.AscUser)
                    .WithMany(p => p.AscUserApprovalMaster)
                    .HasForeignKey(d => d.AscUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_User_Approval_ASC_User");
            });

            modelBuilder.Entity<AscUserMaster>(entity =>
            {
                entity.ToTable("ASC_UserMaster");

                entity.Property(e => e.AscRegionId).HasColumnName("ASC_Region_Id");

                entity.Property(e => e.AscRoleMasterId).HasColumnName("ASC_RoleMaster_Id");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(250);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.UserType)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.HasOne(d => d.AscRegion)
                    .WithMany(p => p.AscUserMaster)
                    .HasForeignKey(d => d.AscRegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_User_ASC_Region");

                entity.HasOne(d => d.AscRoleMaster)
                    .WithMany(p => p.AscUserMaster)
                    .HasForeignKey(d => d.AscRoleMasterId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ASC_User_ASC_RoleMaster");

                entity.HasOne(d => d.Vendor)
                    .WithMany(p => p.AscUserMaster)
                    .HasForeignKey(d => d.VendorId)
                    .HasConstraintName("FK__ASC_UserM__Vendo__36B12243");
            });

            modelBuilder.Entity<AscVendorMaster>(entity =>
            {
                entity.ToTable("ASC_VendorMaster");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasMaxLength(50);

                entity.Property(e => e.Name).IsRequired();
            });
        }
    }
}
