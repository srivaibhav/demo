﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscPromocodeMaster
    {
        public int Id { get; set; }
        public string Promocode { get; set; }
        public int AscVendorId { get; set; }

        public AscVendorMaster AscVendor { get; set; }
    }
}
