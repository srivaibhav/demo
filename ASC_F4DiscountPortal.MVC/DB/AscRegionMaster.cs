﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscRegionMaster
    {
        public AscRegionMaster()
        {
            AscDiscountOrder = new HashSet<AscDiscountOrder>();
            AscUserMaster = new HashSet<AscUserMaster>();
        }

        public int Id { get; set; }
        public string RegionName { get; set; }

        public ICollection<AscDiscountOrder> AscDiscountOrder { get; set; }
        public ICollection<AscUserMaster> AscUserMaster { get; set; }
    }
}
