﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscApprovalTransaction
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string RequestedBy { get; set; }
        public string ApproverEmail { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Comments { get; set; }
        public int AscDiscountOrderId { get; set; }

        public AscDiscountOrder AscDiscountOrder { get; set; }
    }
}
