﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscRoleMaster
    {
        public AscRoleMaster()
        {
            AscUserMaster = new HashSet<AscUserMaster>();
        }

        public int Id { get; set; }
        public string RoleName { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime ModifedDate { get; set; }

        public ICollection<AscUserMaster> AscUserMaster { get; set; }
    }
}
