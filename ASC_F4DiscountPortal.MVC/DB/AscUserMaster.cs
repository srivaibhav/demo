﻿using System;
using System.Collections.Generic;

namespace ASC_F4DiscountPortal.MVC.DB
{
    public partial class AscUserMaster
    {
        public AscUserMaster()
        {
            AscDiscountOrder = new HashSet<AscDiscountOrder>();
            AscUserApprovalMaster = new HashSet<AscUserApprovalMaster>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string City { get; set; }
        public int AscRoleMasterId { get; set; }
        public int AscRegionId { get; set; }
        public string UserType { get; set; }
        public int? VendorId { get; set; }

        public AscRegionMaster AscRegion { get; set; }
        public AscRoleMaster AscRoleMaster { get; set; }
        public AscVendorMaster Vendor { get; set; }
        public ICollection<AscDiscountOrder> AscDiscountOrder { get; set; }
        public ICollection<AscUserApprovalMaster> AscUserApprovalMaster { get; set; }
    }
}
