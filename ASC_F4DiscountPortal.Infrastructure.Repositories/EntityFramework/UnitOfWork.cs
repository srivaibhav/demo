﻿using ASC_F4DiscountPortal.MVC.DB;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASC_F4DiscountPortal.Infrastructure.Repositories.EntityFramework
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly F4DsicountPortalContext _context;
        public UnitOfWork(F4DsicountPortalContext context)
        {
            _context = context;
        }
        //Name should be same as per IUnitOfWork declaration
        public IAscApprovalTransactionRepository AscApprovalTransactionRepository =>
            new Lazy<AscApprovalTransactionRepository>(() => new AscApprovalTransactionRepository(_context)).Value;

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        private bool _disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing)
                    _context.Dispose();

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
