﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using ASC_F4DiscountPortal.MVC.DB;

namespace ASC_F4DiscountPortal.Infrastructure.Repositories
{
    public interface IAscApprovalTransactionRepository: IRepository<AscApprovalTransaction>
    {
        void AddOrUpdate(AscApprovalTransaction address);
    }
}
