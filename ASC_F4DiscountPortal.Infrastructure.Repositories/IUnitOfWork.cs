﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ASC_F4DiscountPortal.Infrastructure.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        IAscApprovalTransactionRepository AscApprovalTransactionRepository { get;}
        int Complete();
        Task<int> CompleteAsync();
    }
}
